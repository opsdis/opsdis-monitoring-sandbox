Opsdis Monitoring Sandbox - Influx TICK, Prometheus and Grafana
------------------------------------------

> This repository is based on the https://github.com/influxdata/sandbox with the addition of Grafana and Prometheus.

This repo is a quick way to get the entire TICK Stack and Grafana spun up and working together. It uses [Docker](https://www.docker.com/) to spin up the full TICK stack and Grafan in a connected fashion. 

To get started you need a running Docker installation. If you don't have one, you can download Docker for [Mac](https://www.docker.com/docker-mac) or [Windows](https://www.docker.com/docker-windows), or follow the installation instructions for Docker CE for your [Linux distribution](https://docs.docker.com/engine/installation/#server).

# Overview

![Overview](https://bitbucket.org/opsdis/opsdis-monitoring-sandbox/raw/master/doc/overview.png)

Out of the box the following setup are done:

- The telegraf data is pushed to influxdb and scraped by prometheus. This means that the same data exists in both the influxdb database and in the prometheus database.
- Grafana is setup up with datastores for Prometheus and Influxdb using datastore provisioning. 
- Grafana is preloaded with additional plugins
    - grafana-clock-panel
    - briangann-gauge-panel
    - natel-plotly-panel
    - grafana-simple-json-datasource
- Grafana is setup with some pre created dashboards that are loaded using Grafana dashboard provisioning.

> At start `grafana` may take some extra seconds to be ready due to loading plugins and data sources

# Running 

To run the `sandbox`, simply use the convenient cli:

> The sandbox.bat script has not been updated to handle Grafana, Prometheus, etc.

```bash
$ ./sandbox
sandbox commands:
  up           -> spin up the sandbox environment
  down         -> tear down the sandbox environment
  restart      -> restart the sandbox
  influxdb     -> attach to the influx cli
  
  enter (influxdb||kapacitor||chronograf||telegraf||ifql||grafana||prometheus||alertmanager) -> enter the specified container
  logs  (influxdb||kapacitor||chronograf||telegraf||ifql||grafana||prometheus||alertmanager) -> stream logs for the specified container
  
  delete-data  -> delete all data created by the TICK Stack and Prometheus
  delete-data-grafana -> delete all config and data created by Grafana
  docker-clean -> stop and remove all running Docker containers and images
  rebuild-docs -> rebuild the documentation image
```

> The delete-data and delete-data-grafana must be run as sudo since data is created with different userid then your own.

To get started just run `./sandbox up`. You browser will open two tabs:

- `localhost:8888` - Chronograf's address. You will use this as a management UI for the full stack
- `localhost:3000` - Grafana's address. Username `admin` and password `foobar`
- `localhost:9090` - Prometheus's address.
- `localhost:9093` - Alertmanager's address.
- `localhost:3010` - TICK documentation server. This contains a simple markdown server for tutorials and documentation.

> NOTE: Make sure to stop any existing installations of `influxdb`, `kapacitor`, `chronograf` or `grafana`. If you have them running the sandbox will run into port conflicts and fail to properly start. In this case stop the existing processes and run `./sandbox restart`. Also make sure you are **not** using _Docker Toolbox_.

# Directory structure

This is not a complete listing but the important parts to start hacking.

```text
sandbox/
├── alertmanager
│   └── config
│       └── config.yml 
├── chronograf
├── docker-compose.yml 
├── grafana
│   └── config
│       ├── dashboards
│       │   └── docker_net.json - a pre created dashboard
│       ├── env.grafana - environment varaibles for grafana on startup
│       ├── grafana.ini - main grafana configuration
│       └── provisioning
│           ├── dashboards
│           │   └── default.yml - define where pre created dashboards are located
│           └── datasources
│               └── datasources.yml - pre created data sources for Influxdb and Prometheus 
├── influxdb
│   └── config
│       └── influxdb.conf
├── kapacitor
│   └── config
│       └── kapacitor.conf
├── prometheus
│   └── config
│       └── prometheus.yml
├── README.md
├── sandbox
├── telegraf
    └── telegraf.conf
```

# Handy Docker commands

What containers are running

    docker ps 
    
Remove images 

    docker rmi <image name>
    
# License 

This software licensed under The MIT License (MIT), https://en.wikipedia.org/wiki/MIT_License
The orignal software was cloned from https://github.com/influxdata/sandbox 
